* [Dawn M. Adams](#dawn-m-adams)
* [Les R. Adams](#les-r-adams)
* [Lashrecse D. Aird](#lashrecse-d-aird)
* [Terry L. Austin](#terry-l-austin)
* [Hala S. Ayala](#hala-s-ayala)
* [Lamont Bagby](#lamont-bagby)
* [John J. Bell](#john-j-bell)
* [Richard P. Bell](#richard-p-bell)
* [Robert B. Bell](#robert-b-bell)
* [Robert S. Bloxom](#robert-s-bloxom)
* [Jeffrey M. Bourne](#jeffrey-m-bourne)
* [Emily M. Brewer](#emily-m-brewer)
* [David L. Bulova](#david-l-bulova)
* [Kathy J. Byron](#kathy-j-byron)
* [Jeffrey L. Campbell](#jeffrey-l-campbell)
* [Ronnie R. Campbell](#ronnie-r-campbell)
* [Betsy B. Carr](#betsy-b-carr)
* [Jennifer Carroll Foy](#jennifer-carroll-foy)
* [Lee J. Carter](#lee-j-carter)
* [Mark L. Cole](#mark-l-cole)
* [Christopher E. Collins](#christopher-e-collins)
* [Kelly K. Convirs-Fowler](#kelly-k-convirs-fowler)
* [M. Kirkland Cox](#m-kirkland-cox)
* [Glenn R. Davis](#glenn-r-davis)
* [Karrie K. Delaney](#karrie-k-delaney)
* [James E. Edmunds](#james-e-edmunds)
* [C. Matthew Fariss](#c-matthew-fariss)
* [Eileen Filler-Corn](#eileen-filler-corn)
* [Hyland F. "Buddy" Fowler](#hyland-f-buddy-fowler)
* [Nicholas J. Freitas](#nicholas-j-freitas)
* [T. Scott Garrett](#t-scott-garrett)
* [C. Todd Gilbert](#c-todd-gilbert)
* [Wendy W. Gooditis](#wendy-w-gooditis)
* [Elizabeth R. Guzman](#elizabeth-r-guzman)
* [C.E. Cliff Hayes](#ce-cliff-hayes)
* [Christopher T. Head](#christopher-t-head)
* [Gordon C. Helsel](#gordon-c-helsel)
* [Steve E. Heretick](#steve-e-heretick)
* [Charniele L. Herring](#charniele-l-herring)
* [M. Keith Hodges](#m-keith-hodges)
* [Patrick A. Hope](#patrick-a-hope)
* [Timothy D. Hugo](#timothy-d-hugo)
* [Chris L. Hurst](#chris-l-hurst)
* [Riley E. Ingram](#riley-e-ingram)
* [Matthew James](#matthew-james)
* [Jerrauld C. "Jay" Jones](#jerrauld-c-jay-jones)
* [S. Chris Jones](#s-chris-jones)
* [Mark L. Keam](#mark-l-keam)
* [Terry G. Kilgore](#terry-g-kilgore)
* [Barry D. Knight](#barry-d-knight)
* [Kaye Kory](#kaye-kory)
* [Paul E. Krizek](#paul-e-krizek)
* [R. Steven Landes](#r-steven-landes)
* [Dave A. LaRock](#dave-a-larock)
* [James A. "Jay" Leftwich](#james-a-jay-leftwich)
* [Mark H. Levine](#mark-h-levine)
* [Joseph C. Lindsey](#joseph-c-lindsey)
* [Alfonso H. Lopez](#alfonso-h-lopez)
* [Daniel W. Marshall](#daniel-w-marshall)
* [John J. McGuire](#john-j-mcguire)
* [Joseph P. McNamara](#joseph-p-mcnamara)
* [Delores L. McQuinn](#delores-l-mcquinn)
* [Jason S. Miyares](#jason-s-miyares)
* [James W. Morefield](#james-w-morefield)
* [Michael P. Mullin](#michael-p-mullin)
* [Kathleen Murphy](#kathleen-murphy)
* [Israel D. O'Quinn](#israel-d-o'quinn)
* [Robert D. Orrock](#robert-d-orrock)
* [Christopher K. Peace](#christopher-k-peace)
* [Todd E. Pillion](#todd-e-pillion)
* [Kenneth R. Plum](#kenneth-r-plum)
* [Brenda L. Pogge](#brenda-l-pogge)
* [Charles D. Poindexter](#charles-d-poindexter)
* [Marcia S. "Cia" Price](#marcia-s-cia-price)
* [Margaret B. Ransone](#margaret-b-ransone)
* [Sam Rasoul](#sam-rasoul)
* [David A. Reid](#david-a-reid)
* [Roxann L. Robinson](#roxann-l-robinson)
* [Debra H. Rodman](#debra-h-rodman)
* [Danica A. Roem](#danica-a-roem)
* [Nick Rush](#nick-rush)
* [Mark D. Sickles](#mark-d-sickles)
* [Marcus B. Simon](#marcus-b-simon)
* [Christopher P. Stolle](#christopher-p-stolle)
* [Richard C. "Rip" Sullivan](#richard-c-rip-sullivan)
* [Robert M. "Bob" Thomas](#robert-m-bob-thomas)
* [Luke E. Torian](#luke-e-torian)
* [David J. Toscano](#david-j-toscano)
* [Kathy K.L. Tran](#kathy-kl-tran)
* [Cheryl B. Turpin](#cheryl-b-turpin)
* [Roslyn C. Tyler](#roslyn-c-tyler)
* [Schuyler T. VanValkenburg](#schuyler-t-vanvalkenburg)
* [Jeion A. Ward](#jeion-a-ward)
* [R. Lee Ware](#r-lee-ware)
* [Vivian E. Watts](#vivian-e-watts)
* [Michael J. Webert](#michael-j-webert)
* [Tony O. Wilt](#tony-o-wilt)
* [Thomas C. Wright](#thomas-c-wright)
* [David E. Yancey](#david-e-yancey)
* [George L. Barker](#george-l-barker)
* [Richard H. Black](#richard-h-black)
* [Jennifer B. Boysko](#jennifer-b-boysko)
* [Charles W. Carrico](#charles-w-carrico)
* [A. Benton "Ben" Chafin](#a-benton-ben-chafin)
* [Amanda F. Chase](#amanda-f-chase)
* [John A. Cosgrove](#john-a-cosgrove)
* [Rosalyn R. Dance](#rosalyn-r-dance)
* [R. Creigh Deeds](#r-creigh-deeds)
* [Bill R. DeSteph](#bill-r-desteph)
* [Siobhan S. Dunnavant](#siobhan-s-dunnavant)
* [Adam P. Ebbin](#adam-p-ebbin)
* [John S. Edwards](#john-s-edwards)
* [Barbara A. Favola](#barbara-a-favola)
* [Emmett W. Hanger](#emmett-w-hanger)
* [Janet D. Howell](#janet-d-howell)
* [Lynwood W. Lewis](#lynwood-w-lewis)
* [Mamie E. Locke](#mamie-e-locke)
* [L. Louise Lucas](#l-louise-lucas)
* [David W. Marsden](#david-w-marsden)
* [T. Montgomery "Monty" Mason](#t-montgomery-monty-mason)
* [Jennifer L. McClellan](#jennifer-l-mcclellan)
* [Ryan T. McDougle](#ryan-t-mcdougle)
* [Jeremy S. McPike](#jeremy-s-mcpike)
* [Stephen D. Newman](#stephen-d-newman)
* [Thomas K. Norment](#thomas-k-norment)
* [Mark D. Obenshain](#mark-d-obenshain)
* [Mark J. Peake](#mark-j-peake)
* [J. Chapman Petersen](#j-chapman-petersen)
* [Bryce E. Reeves](#bryce-e-reeves)
* [Frank M. Ruff](#frank-m-ruff)
* [Richard L. Saslaw](#richard-l-saslaw)
* [Lionell Spruill](#lionell-spruill)
* [William M. Stanley](#william-m-stanley)
* [Richard H. Stuart](#richard-h-stuart)
* [Glen H. Sturtevant](#glen-h-sturtevant)
* [David R. Suetterlein](#david-r-suetterlein)
* [Scott A. Surovell](#scott-a-surovell)
* [Jill Holtzman Vogel](#jill-holtzman-vogel)
* [Frank W. Wagner](#frank-w-wagner)

## Dawn M. Adams

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Les R. Adams

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Lashrecse D. Aird

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Terry L. Austin

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Hala S. Ayala

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Lamont Bagby

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## John J. Bell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Richard P. Bell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Robert B. Bell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-21T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 250.0 | 500.0 | 
| 2014-09-22T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2013-11-05T00:00:00.000-05:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 


 
[[back to top]](#top)

## Robert S. Bloxom

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jeffrey M. Bourne

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Emily M. Brewer

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## David L. Bulova

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Kathy J. Byron

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-21T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2015-10-19T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 500.0 | 
| 2014-10-03T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2013-10-29T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 650.0 | 
| 2012-09-27T00:00:00.000-04:00 | Exxon Mobil Corporation | 3225 Gallows Rd. Fairfax VA 22037 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Jeffrey L. Campbell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Ronnie R. Campbell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Betsy B. Carr

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jennifer Carroll Foy

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Lee J. Carter

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Mark L. Cole

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Christopher E. Collins

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Kelly K. Convirs-Fowler

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## M. Kirkland Cox

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-11-01T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 500.0 | 500.0 | 
| 2015-10-16T00:00:00.000-04:00 | Exxon Mobile Corporation | 3225 Gallows Rd. Fairfax VA 22037 | 1000.0 | 2000.0 | 
| 2014-10-06T00:00:00.000-04:00 | Exxon Mobile Corporation | 3225 Gallows Rd. Fairfax VA 22037 | 1000.0 | 1000.0 | 
| 2013-11-12T00:00:00.000-05:00 | Exxon Mobile Corporation | 3225 Gallows Rd. Fairfax VA 22037 | 1000.0 | 2000.0 | 
| 2012-09-28T00:00:00.000-04:00 | Exxon Mobile Corporation | 3225 Gallows Rd. Fairfax VA 22037 | 1000.0 | 1000.0 | 


 
[[back to top]](#top)

## Glenn R. Davis

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Karrie K. Delaney

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## James E. Edmunds

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## C. Matthew Fariss

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Eileen Filler-Corn

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Hyland F. "Buddy" Fowler

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Nicholas J. Freitas

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## T. Scott Garrett

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## C. Todd Gilbert

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-18T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2015-10-01T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 500.0 | 
| 2014-09-29T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2013-11-08T00:00:00.000-05:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 650.0 | 
| 2012-09-28T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Wendy W. Gooditis

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Elizabeth R. Guzman

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## C.E. Cliff Hayes

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Christopher T. Head

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Gordon C. Helsel

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Steve E. Heretick

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Charniele L. Herring

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## M. Keith Hodges

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Patrick A. Hope

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Timothy D. Hugo

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-20T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O.Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2015-10-26T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O.Box 2519 Houston TX 77252 | 500.0 | 750.0 | 
| 2014-10-06T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O.Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2013-11-06T00:00:00.000-05:00 | Exxon Mobil Corporation | P.O.Box 2519 Houston TX 77252 | 250.0 | 650.0 | 
| 2013-09-27T00:00:00.000-04:00 | R&N Corp. T/A Duke Street Exxon | 2838 Duke St Alexandria VA 22314 | 200.0 | 200.0 | 
| 2012-09-28T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O.Box 2519 Houston TX 77252 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Chris L. Hurst

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Riley E. Ingram

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Matthew James

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jerrauld C. "Jay" Jones

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## S. Chris Jones

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-21T00:00:00.000-04:00 | Exxon Mobil Corporation | P. O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2015-10-23T00:00:00.000-04:00 | Exxon Mobil Corporation | P. O. Box 2519 Houston TX 77252 | 250.0 | 500.0 | 
| 2014-10-10T00:00:00.000-04:00 | Exxon Mobil Corporation | P. O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2013-11-15T00:00:00.000-05:00 | Exxon Mobil Corporation | P. O. Box 2519 Houston TX 77252 | 250.0 | 250.0 | 


 
[[back to top]](#top)

## Mark L. Keam

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-16T00:00:00.000-04:00 | Exxon MobilCorp | 1201 K St Ste 1920 Sacramento CA 95814-3919 | 250.0 | 250.0 | 


 
[[back to top]](#top)

## Terry G. Kilgore

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-11-10T00:00:00.000-05:00 | Exxon Mobil | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2015-10-14T00:00:00.000-04:00 | Exxon Mobil | PO Box 2519 Houston TX 77252 | 500.0 | 750.0 | 
| 2014-10-06T00:00:00.000-04:00 | Exxon Mobil | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2013-11-18T00:00:00.000-05:00 | Exxon Mobil | PO Box 2519 Houston TX 77252 | 500.0 | 900.0 | 
| 2012-10-01T00:00:00.000-04:00 | Exxon Mobil | PO Box 2519 Houston TX 77252 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Barry D. Knight

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Kaye Kory

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Paul E. Krizek

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## R. Steven Landes

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Dave A. LaRock

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## James A. "Jay" Leftwich

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Mark H. Levine

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Joseph C. Lindsey

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Alfonso H. Lopez

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Daniel W. Marshall

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-21T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252-2519 | 250.0 | 250.0 | 
| 2015-10-20T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252-2519 | 250.0 | 500.0 | 
| 2014-10-06T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252-2519 | 250.0 | 250.0 | 
| 2013-11-21T00:00:00.000-05:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252-2519 | 250.0 | 650.0 | 
| 2012-09-25T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252-2519 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## John J. McGuire

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Joseph P. McNamara

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Delores L. McQuinn

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jason S. Miyares

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## James W. Morefield

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Michael P. Mullin

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Kathleen Murphy

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Israel D. O'Quinn

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-29T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 


 
[[back to top]](#top)

## Robert D. Orrock

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Christopher K. Peace

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Todd E. Pillion

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Kenneth R. Plum

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Brenda L. Pogge

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Charles D. Poindexter

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Marcia S. "Cia" Price

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Margaret B. Ransone

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Sam Rasoul

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## David A. Reid

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Roxann L. Robinson

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Debra H. Rodman

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Danica A. Roem

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Nick Rush

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Mark D. Sickles

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Marcus B. Simon

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Christopher P. Stolle

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Richard C. "Rip" Sullivan

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Robert M. "Bob" Thomas

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Luke E. Torian

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## David J. Toscano

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-11-02T00:00:00.000-05:00 | Exxon Mobil Corporation | Randy T. Smith, US Government Affairs Manager 3225 Gallows Road Fairfax VA 22037-0001 | 250.0 | 250.0 | 
| 2014-10-02T00:00:00.000-04:00 | Exxon Mobil Corporation | Randy T. Smith, US Government Affairs Manager 3225 Gallows Road Fairfax VA 22037-0001 | 250.0 | 250.0 | 
| 2013-11-15T00:00:00.000-05:00 | Exxon Mobil Corporation | Randy T. Smith, US Government Affairs Manager 3225 Gallows Road Fairfax VA 22037 | 250.0 | 650.0 | 
| 2012-09-27T00:00:00.000-04:00 | Exxon Mobil Corporation | Randy T. Smith, US Government Affairs Manager 3225 Gallows Road Fairfax VA 22037 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Kathy K.L. Tran

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Cheryl B. Turpin

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Roslyn C. Tyler

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Schuyler T. VanValkenburg

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jeion A. Ward

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## R. Lee Ware

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-28T00:00:00.000-04:00 | Exxon Mobile | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2015-10-06T00:00:00.000-04:00 | Exxon Mobile | PO Box 2519 Houston TX 77252 | 250.0 | 500.0 | 
| 2014-10-13T00:00:00.000-04:00 | Exxon Mobile | PO Box 2519 Houston TX 77252 | 250.0 | 250.0 | 
| 2012-10-06T00:00:00.000-04:00 | Exxon Mobile | PO Box 2519 Houston TX 77252 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Vivian E. Watts

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Michael J. Webert

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Tony O. Wilt

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Thomas C. Wright

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## David E. Yancey

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## George L. Barker

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Richard H. Black

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jennifer B. Boysko

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Charles W. Carrico

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-17T00:00:00.000-04:00 | Exxon Mobil | P.O. Box 2519 Houston TX 77252-2519 | 500.0 | 500.0 | 
| 2015-10-15T00:00:00.000-04:00 | Exxon Mobil | P.O. Box 2519 Houston TX 77252-2519 | 500.0 | 500.0 | 


 
[[back to top]](#top)

## A. Benton "Ben" Chafin

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-19T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 500.0 | 500.0 | 


 
[[back to top]](#top)

## Amanda F. Chase

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## John A. Cosgrove

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-17T00:00:00.000-04:00 | Exxon | P.O. Box 2519 Houston TX 77252 | 500.0 | 500.0 | 


 
[[back to top]](#top)

## Rosalyn R. Dance

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-16T00:00:00.000-04:00 | Exxon Mobil Corporation | P O Box 2519 Houston TX 77252 | 500.0 | 500.0 | 


 
[[back to top]](#top)

## R. Creigh Deeds

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Bill R. DeSteph

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Siobhan S. Dunnavant

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Adam P. Ebbin

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## John S. Edwards

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Barbara A. Favola

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Emmett W. Hanger

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Janet D. Howell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Lynwood W. Lewis

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Mamie E. Locke

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## L. Louise Lucas

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## David W. Marsden

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## T. Montgomery "Monty" Mason

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jennifer L. McClellan

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Ryan T. McDougle

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2013-11-18T00:00:00.000-05:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 250.0 | 650.0 | 
| 2012-10-11T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Jeremy S. McPike

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Stephen D. Newman

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-10-16T00:00:00.000-04:00 | Exxon Mobile | 3225 Gallows Road Fairfax VA 22037 | 500.0 | 1150.0 | 
| 2013-12-02T00:00:00.000-05:00 | Exxon Mobile | 3225 Gallows Road Fairfax VA 22037 | 250.0 | 650.0 | 
| 2012-09-28T00:00:00.000-04:00 | Exxon Mobile | 3225 Gallows Road Fairfax VA 22037 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Thomas K. Norment

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-24T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 500.0 | 500.0 | 
| 2013-11-15T00:00:00.000-05:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 1000.0 | 2000.0 | 
| 2012-09-16T00:00:00.000-04:00 | Exxon Mobil Corporation | P.O. Box 2519 Houston TX 77252 | 1000.0 | 1000.0 | 


 
[[back to top]](#top)

## Mark D. Obenshain

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-11-14T00:00:00.000-05:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 500.0 | 500.0 | 
| 2015-10-21T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 500.0 | 1400.0 | 
| 2014-10-22T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 250.0 | 900.0 | 
| 2013-11-20T00:00:00.000-05:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 250.0 | 650.0 | 
| 2012-11-14T00:00:00.000-05:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Mark J. Peake

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## J. Chapman Petersen

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2013-01-04T00:00:00.000-05:00 | VanDorn Exxon | 4008 Genessee Place #101 Woodbridge VA 22192 | 250.0 | 250.0 | 


 
[[back to top]](#top)

## Bryce E. Reeves

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Frank M. Ruff

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Richard L. Saslaw

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Lionell Spruill

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## William M. Stanley

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2015-11-05T00:00:00.000-05:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 77252 | 500.0 | 500.0 | 


 
[[back to top]](#top)

## Richard H. Stuart

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2013-11-17T00:00:00.000-05:00 | Exxon Mobile Corporation | 3225 Gallow Road Fairfax VA 22037 | 250.0 | 650.0 | 
| 2012-09-28T00:00:00.000-04:00 | Exxon Mobile Corporation | 3225 Gallow Road Fairfax VA 22037 | 400.0 | 400.0 | 


 
[[back to top]](#top)

## Glen H. Sturtevant

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## David R. Suetterlein

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Scott A. Surovell

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Jill Holtzman Vogel

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |


 
[[back to top]](#top)

## Frank W. Wagner

| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |
| -- | -- | -- | -- | -- |
| 2016-10-21T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 772522519 | 500.0 | 500.0 | 
| 2015-10-20T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 772522519 | 500.0 | 500.0 | 
| 2014-10-07T00:00:00.000-04:00 | Exxon Mobil Corporation | PO Box 2519 Houston TX 772522519 | 250.0 | 250.0 | 
| 2013-11-27T00:00:00.000-05:00 | Exxon Mobil Corporation | 3225 Gallows Road Fairfax VA 22037 | 250.0 | 650.0 | 
| 2012-09-25T00:00:00.000-04:00 | Exxon Mobil Corporation | 3225 Gallows Road Fairfax VA 22037 | 400.0 | 400.0 | 


 
[[back to top]](#top)

