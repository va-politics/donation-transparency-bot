package com.gitlab.ccook;

import com.gitlab.ccook.model.*;
import com.gitlab.ccook.util.Option;
import com.gitlab.ccook.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class CampaignFinanceReportDownloader {

    private static Logger log = LoggerFactory.getLogger(CampaignFinanceReportDownloader.class);

    public static void main(String[] args) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter("transurban.txt", true));
        GAMemberFinder finder = new GAMemberFinder();
        for (GAMember member : finder.getMembers()) {
            String name = member.getName();
            String slug = member.getName().replace(".","")
                    .replace(" ","-").replace(",","").replace("\"","").toLowerCase();
            writer.append("* "+"["+name+"]"+"(#"+slug+")\n");
        }
        writer.append("\n");


        for (GAMember member : finder.getMembers()) {
            String cmtID = member.getLink();
            System.out.println("----" + member.getName() + "-----");
            writer.append("## "+member.getName()+"\n\n");
            writer.append("| Transaction Date | Corp Name | Corp Address | Amount | Total to Date |\n");
            writer.append("| -- | -- | -- | -- | -- |\n");
            CampaignReportSearcher s = new CampaignReportSearcher(cmtID);
            List<CampaignFinanceReport> campaignFinanceInfo = s.getCampaignFinanceInfo(cmtID);
            System.out.println(campaignFinanceInfo.size());
            for (CampaignFinanceReport report : campaignFinanceInfo) {
                if (!report.getHeader().isAmendment()) {
                    Option<CampaignFinanceReportScheduleA> scheduleAOption = report.getScheduleA();
                    if (scheduleAOption.isDefined()) {
                        System.out.println(scheduleAOption);
                        CampaignFinanceReportScheduleA scheduleA = scheduleAOption.get();
                        for (Pair<Contributor, TransactionData> contribution : scheduleA.getContributions()) {
                            Contributor first = contribution.getFirst();
                            TransactionData second = contribution.getSecond();
                            if (!first.isIndividual()) {
                                if( first.getName().toLowerCase().contains("trans") && (first.getName().toLowerCase().contains("urban"))){
                                    String line = "| "+second.getTransactionDate()+" | "+first.getName()+" | "+ first.getAddress().getFullAddress()+" | "+second.getAmount()+" | "+second.getTotalToDate()+" | ";
                                    writer.append(line+"\n");
                                    writer.flush();
                                }
                            }
                        }
                    }
                }
            }
            writer.append("\n\n \n" +
                    "[[back to top]](#top)\n\n");
            System.out.println("---------------------");
        }
        writer.flush();
        writer.close();
        ;
/*        for (GAMember houseMember : finder.getHouseMembers()) {
            CampaignReportSearcher s = new CampaignReportSearcher(houseMember.getFirst() + " " + houseMember.getLast());
            CommitteeSearchResult committeeInfo = s.getCommitteeInfo();
            if (committeeInfo.getCommittees().size() > 0) {
*//*                List<CampaignFinanceReport> campaignFinanceInfo = s.getCampaignFinanceInfo(committeeInfo.getCommittees().get(0).AccountId);
                for (CampaignFinanceReport report : campaignFinanceInfo) {
                    log.info(report.toString());
                }*//*
            } else {
                log.warn("None found {}", houseMember);
                log.info(committeeInfo.toString());
                System.exit(0);
            }
        }*/

    }
}
