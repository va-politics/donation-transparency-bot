package com.gitlab.ccook;


import com.gitlab.ccook.model.GAMember;
import com.gitlab.ccook.util.Option;
import com.gitlab.ccook.util.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class GAMemberFinder {

    private int sessionNumber;
    List<GAMember> members = new ArrayList<>();

    public GAMemberFinder() {
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/campaign_master_list.txt");
        Scanner in = new Scanner(resourceAsStream);
        in.useDelimiter("\n");
        while (in.hasNext()) {
            String next = in.next();
            String[] parts = next.split(",");
            Option<String> twitterHandle = new Option<>();
            if (parts.length == 3) {
                twitterHandle = new Option<>(parts[2]);
            }
            members.add(new GAMember(parts[0], twitterHandle, parts[1]));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GAMemberFinder that = (GAMemberFinder) o;
        return sessionNumber == that.sessionNumber &&
                Objects.equals(members, that.members);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionNumber, members);
    }

    @Override
    public String toString() {
        return "GAMemberFinder{" +
                "sessionNumber=" + sessionNumber +
                ", members=" + members +
                '}';
    }

    public int getSessionNumber() {
        return sessionNumber;
    }

    public List<GAMember> getMembers() {
        return members;
    }
}