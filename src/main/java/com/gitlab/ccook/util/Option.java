package com.gitlab.ccook.util;

import java.util.Objects;

public class Option<A> {
    private A possiblyNullThing;

    public Option(A possiblyNullThing) {
        this.possiblyNullThing = possiblyNullThing;
    }

    public Option() {
    }

    public boolean isDefined() {
        return null != possiblyNullThing;
    }

    public A get() {
        return possiblyNullThing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Option<?> option = (Option<?>) o;
        return Objects.equals(possiblyNullThing, option.possiblyNullThing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(possiblyNullThing);
    }

    @Override
    public String toString() {
        return "Option{" +
                "possiblyNullThing=" + possiblyNullThing +
                '}';
    }
}
