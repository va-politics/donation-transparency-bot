package com.gitlab.ccook.model;

import com.gitlab.ccook.util.Option;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class CampaignFinanceReport {
    private String url = "http://cfreports.sbe.virginia.gov";
    private CampaignFinanceReportHeader header;
    private Option<CampaignFinanceReportScheduleA> scheduleA = new Option<>();
    private Option<CampaignFinanceReportScheduleB> scheduleB = new Option<>();
    private Option<CampaignFinanceReportScheduleC> scheduleC = new Option<>();
    private Option<CampaignFinanceReportScheduleD> scheduleD = new Option<>();
    private Option<CampaignFinanceReportScheduleE> scheduleE = new Option<>();
    private Option<CampaignFinanceReportScheduleF> scheduleF = new Option<>();
    private Option<CampaignFinanceReportScheduleG> scheduleG = new Option<>();
    private Option<CampaignFinanceReportScheduleH> scheduleH = new Option<>();

    public CampaignFinanceReport(String link) {
        url = url + link;
        Document doc = null;
        try {
            doc = Jsoup.parse(new URL(url), 1 * 6 * 1000);
        } catch (IOException e) {
            e.toString();
        }
        header = new CampaignFinanceReportHeader(doc.select("ReportHeader").iterator().next());
        if (!doc.select("ScheduleA").iterator().next().text().trim().isEmpty()) {
            scheduleA = new Option<>(new CampaignFinanceReportScheduleA(doc.select("ScheduleA").iterator().next()));
        }
    }

    public String getUrl() {
        return url;
    }

    public CampaignFinanceReportHeader getHeader() {
        return header;
    }

    public Option<CampaignFinanceReportScheduleA> getScheduleA() {
        return scheduleA;
    }

    public Option<CampaignFinanceReportScheduleB> getScheduleB() {
        return scheduleB;
    }

    public Option<CampaignFinanceReportScheduleC> getScheduleC() {
        return scheduleC;
    }

    public Option<CampaignFinanceReportScheduleD> getScheduleD() {
        return scheduleD;
    }

    public Option<CampaignFinanceReportScheduleE> getScheduleE() {
        return scheduleE;
    }

    public Option<CampaignFinanceReportScheduleF> getScheduleF() {
        return scheduleF;
    }

    public Option<CampaignFinanceReportScheduleG> getScheduleG() {
        return scheduleG;
    }

    public Option<CampaignFinanceReportScheduleH> getScheduleH() {
        return scheduleH;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignFinanceReport that = (CampaignFinanceReport) o;
        return Objects.equals(url, that.url) &&
                Objects.equals(header, that.header) &&
                Objects.equals(scheduleA, that.scheduleA) &&
                Objects.equals(scheduleB, that.scheduleB) &&
                Objects.equals(scheduleC, that.scheduleC) &&
                Objects.equals(scheduleD, that.scheduleD) &&
                Objects.equals(scheduleE, that.scheduleE) &&
                Objects.equals(scheduleF, that.scheduleF) &&
                Objects.equals(scheduleG, that.scheduleG) &&
                Objects.equals(scheduleH, that.scheduleH);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, header, scheduleA, scheduleB, scheduleC, scheduleD, scheduleE, scheduleF, scheduleG, scheduleH);
    }

    @Override
    public String toString() {
        return "CampaignFinanceReport{" +
                "url='" + url + '\'' +
                ", header=" + header +
                ", scheduleA=" + scheduleA +
                ", scheduleB=" + scheduleB +
                ", scheduleC=" + scheduleC +
                ", scheduleD=" + scheduleD +
                ", scheduleE=" + scheduleE +
                ", scheduleF=" + scheduleF +
                ", scheduleG=" + scheduleG +
                ", scheduleH=" + scheduleH +
                '}';
    }
}
