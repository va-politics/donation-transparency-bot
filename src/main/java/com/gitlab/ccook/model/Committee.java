package com.gitlab.ccook.model;

import java.util.Objects;

public class Committee {
    public String AccountId;
    public String CommitteeCode;
    public String CommitteeName;
    public String CandidateName;
    public String CommitteeType;

    @Override
    public String toString() {
        return "Committee{" +
                "AccountId='" + AccountId + '\'' +
                ", CommitteeCode='" + CommitteeCode + '\'' +
                ", CommitteeName='" + CommitteeName + '\'' +
                ", CandidateName='" + CandidateName + '\'' +
                ", CommitteeType='" + CommitteeType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Committee committee = (Committee) o;
        return Objects.equals(AccountId, committee.AccountId) &&
                Objects.equals(CommitteeCode, committee.CommitteeCode) &&
                Objects.equals(CommitteeName, committee.CommitteeName) &&
                Objects.equals(CandidateName, committee.CandidateName) &&
                Objects.equals(CommitteeType, committee.CommitteeType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(AccountId, CommitteeCode, CommitteeName, CandidateName, CommitteeType);
    }

    public String getAccountId() {
        return AccountId;
    }

    public void setAccountId(String accountId) {
        AccountId = accountId;
    }

    public String getCommitteeCode() {
        return CommitteeCode;
    }

    public void setCommitteeCode(String committeeCode) {
        CommitteeCode = committeeCode;
    }

    public String getCommitteeName() {
        return CommitteeName;
    }

    public void setCommitteeName(String committeeName) {
        CommitteeName = committeeName;
    }

    public String getCandidateName() {
        return CandidateName;
    }

    public void setCandidateName(String candidateName) {
        CandidateName = candidateName;
    }

    public String getCommitteeType() {
        return CommitteeType;
    }

    public void setCommitteeType(String committeeType) {
        CommitteeType = committeeType;
    }
}
