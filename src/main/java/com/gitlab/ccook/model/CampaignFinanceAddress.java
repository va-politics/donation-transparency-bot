package com.gitlab.ccook.model;

import com.gitlab.ccook.util.Option;
import org.jsoup.select.Elements;

import java.util.Objects;

public class CampaignFinanceAddress {
    private String line1 = "";
    private Option<String> line2 = new Option<>();
    private String city = "";
    private String state = "";
    private String zip = "";
    private String fullAddress;

    public CampaignFinanceAddress(Elements address) {
        if (address.select("Line1").iterator().hasNext()) {
            line1 = address.select("Line1").iterator().next().text().trim();
        }
        if (address.select("Line2").iterator().hasNext()) {
            line2 = new Option<>(address.select("Line2").iterator().next().text().trim());
        }
        if (address.select("City").iterator().hasNext()) {
            city = address.select("City").iterator().next().text().trim();
        }
        if (address.select("State").iterator().hasNext()) {
            state = address.select("State").iterator().next().text().trim();
        }
        if (address.select("ZipCode").iterator().hasNext()) {
            zip = address.select("ZipCode").iterator().next().text().trim();
        }
        if (line2.isDefined()) {
            fullAddress = line1 + " " + line2.get() + " " + city + " " + state + " " + zip;
        } else {
            fullAddress = line1 + " " + city + " " + state + " " + zip;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignFinanceAddress that = (CampaignFinanceAddress) o;
        return Objects.equals(line1, that.line1) &&
                Objects.equals(line2, that.line2) &&
                Objects.equals(city, that.city) &&
                Objects.equals(state, that.state) &&
                Objects.equals(zip, that.zip) &&
                Objects.equals(fullAddress, that.fullAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(line1, line2, city, state, zip, fullAddress);
    }

    @Override
    public String toString() {
        return "CampaignFinanceAddress{" +
                "line1='" + line1 + '\'' +
                ", line2=" + line2 +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zip='" + zip + '\'' +
                ", fullAddress='" + fullAddress + '\'' +
                '}';
    }

    public String getLine1() {
        return line1;
    }

    public Option<String> getLine2() {
        return line2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getFullAddress() {
        return fullAddress;
    }
}
