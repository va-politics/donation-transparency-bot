package com.gitlab.ccook.model;

import com.gitlab.ccook.util.Option;
import org.jsoup.select.Elements;

import java.util.Objects;

public class Contributor {
    private boolean isIndividual;
    private String name;
    private CampaignFinanceAddress address;
    private Option<String> nameOfEmployer = new Option<>();
    private Option<String> occupationOrTypeOfBusiness = new Option<>();
    private Option<String> primaryCityStateOfEmploymentOfBusiness = new Option<>();

    public Contributor(Elements contributor) {
        isIndividual = Boolean.parseBoolean(contributor.iterator().next().attr("IsIndividual").trim());
        String prefix = "";
        if (contributor.select("Prefix").iterator().hasNext()) {
            prefix = contributor.select("Prefix").iterator().next().text().trim();
        }
        String firstName = "";
        if (contributor.select("FirstName").iterator().hasNext()) {
            firstName = contributor.select("FirstName").iterator().next().text().trim();
        }
        String middleName = "";
        if (contributor.select("MiddleName").iterator().hasNext()) {
            middleName = contributor.select("MiddleName").iterator().next().text().trim();
        }
        String lastName = "";
        if (contributor.select("LastName").iterator().hasNext()) {
            lastName = contributor.select("LastName").iterator().next().text().trim();
        }
        this.name = (prefix + " " + firstName + " " + middleName + " " + " " + lastName).replace("  ", " ").trim();
        this.address = new CampaignFinanceAddress(contributor.select("Address"));
        if (contributor.select("NameOfEmployer").iterator().hasNext()) {
            this.nameOfEmployer = new Option<>(contributor.select("NameOfEmployer").iterator().next().text().trim());
        }
        if(contributor.select("OccupationOrTypeOfBusiness").iterator().hasNext()){
            this.occupationOrTypeOfBusiness = new Option<>(contributor.select("OccupationOrTypeOfBusiness").iterator().next().text().trim());
        }
        if (contributor.select("PrimaryCityAndStateOfEmploymentOrBusiness").iterator().hasNext()) {
            this.primaryCityStateOfEmploymentOfBusiness = new Option<>(contributor.select("PrimaryCityAndStateOfEmploymentOrBusiness").iterator().next().text().trim());
        }
    }

    @Override
    public String toString() {
        return "Contributor{" +
                "isIndividual=" + isIndividual +
                ", name='" + name + '\'' +
                ", address=" + address +
                ", nameOfEmployer=" + nameOfEmployer +
                ", occupationOrTypeOfBusiness=" + occupationOrTypeOfBusiness +
                ", primaryCityStateOfEmploymentOfBusiness=" + primaryCityStateOfEmploymentOfBusiness +
                '}';
    }

    public boolean isIndividual() {
        return isIndividual;
    }

    public String getName() {
        return name;
    }

    public CampaignFinanceAddress getAddress() {
        return address;
    }

    public Option<String> getNameOfEmployer() {
        return nameOfEmployer;
    }

    public Option<String> getOccupationOrTypeOfBusiness() {
        return occupationOrTypeOfBusiness;
    }

    public Option<String> getPrimaryCityStateOfEmploymentOfBusiness() {
        return primaryCityStateOfEmploymentOfBusiness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contributor that = (Contributor) o;
        return isIndividual == that.isIndividual &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(nameOfEmployer, that.nameOfEmployer) &&
                Objects.equals(occupationOrTypeOfBusiness, that.occupationOrTypeOfBusiness) &&
                Objects.equals(primaryCityStateOfEmploymentOfBusiness, that.primaryCityStateOfEmploymentOfBusiness);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isIndividual, name, address, nameOfEmployer, occupationOrTypeOfBusiness, primaryCityStateOfEmploymentOfBusiness);
    }
}
