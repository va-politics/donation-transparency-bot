package com.gitlab.ccook.model;

import com.gitlab.ccook.util.BotUtils;
import org.joda.time.DateTime;
import org.jsoup.nodes.Element;

import java.util.Objects;

public class TransactionData {
    private DateTime transactionDate;
    private Double amount;
    private Double totalToDate;
    public TransactionData(Element contributor) {
        this.transactionDate = BotUtils.parseDate(contributor.select("TransactionDate").iterator().next().text().trim());
        this.amount =  Double.parseDouble(contributor.select("Amount").iterator().next().text().trim());
        this.totalToDate =  Double.parseDouble(contributor.select("TotalToDate").iterator().next().text().trim());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionData that = (TransactionData) o;
        return Objects.equals(transactionDate, that.transactionDate) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(totalToDate, that.totalToDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionDate, amount, totalToDate);
    }

    @Override
    public String toString() {
        return "TransactionData{" +
                "transactionDate=" + transactionDate +
                ", amount=" + amount +
                ", totalToDate=" + totalToDate +
                '}';
    }

    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public Double getAmount() {
        return amount;
    }

    public Double getTotalToDate() {
        return totalToDate;
    }
}
